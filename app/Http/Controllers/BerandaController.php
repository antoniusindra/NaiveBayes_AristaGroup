<?php

namespace App\Http\Controllers;

use App\Dataset;
use App\NilaiAtribut;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
    private $linkView = 'modul.';

    public function __construct()
    {
        $this->middleware('Admin');
    }

    public function index()
    {
    	//data grafik column bar produk
        $dataBarProduk = array();
        $listProduk = NilaiAtribut::distinct('nilai_atribut')->where('nama_atribut', 'Produk')->get();
        foreach ($listProduk as $view) {
            $jumData[] = Dataset::where('produk', $view->nilai_atribut)->count();
            $row2 = ['name' => $view->nilai_atribut, 'data' => $jumData];
            array_push($dataBarProduk,$row2);
            unset($jumData);
        }
        $jsonProduk = json_encode($dataBarProduk);

        //data grafik column bar color
        $dataBarColor = array();
        $listColor = NilaiAtribut::distinct('nilai_atribut')->where('nama_atribut', 'Color')->get();
        foreach ($listColor as $view) {
            $jumData[] = Dataset::where('color', $view->nilai_atribut)->count();
            $row2 = ['name' => $view->nilai_atribut, 'data' => $jumData];
            array_push($dataBarColor,$row2);
            unset($jumData);
        }
        $jsonColor = json_encode($dataBarColor);

        //data grafik column bar alamat customer
        $dataBarAlamatCustomer = array();
        $listAlamatCustomer = NilaiAtribut::distinct('nilai_atribut')->where('nama_atribut', 'Alamat Customer')->get();
        foreach ($listAlamatCustomer as $view) {
            $jumData[] = Dataset::where('alamat_customer', $view->nilai_atribut)->count();
            $row2 = ['name' => $view->nilai_atribut, 'data' => $jumData];
            array_push($dataBarAlamatCustomer,$row2);
            unset($jumData);
        }
        $jsonAlamatCustomer = json_encode($dataBarAlamatCustomer);

    	//data grafik column bar selera konsumen
        $dataBarSeleraKonsumen = array();
        $listSeleraKonsumen = NilaiAtribut::distinct('nilai_atribut')->where('nama_atribut', 'Selera Konsumen')->get();
        foreach ($listSeleraKonsumen as $view) {
            $jumData[] = Dataset::where('selera_konsumen', $view->nilai_atribut)->count();
            $row2 = ['name' => $view->nilai_atribut, 'data' => $jumData];
            array_push($dataBarSeleraKonsumen,$row2);
            unset($jumData);
        }
        $jsonSeleraKonsumen = json_encode($dataBarSeleraKonsumen);

        return view($this->linkView.'beranda', compact('jsonProduk', 'jsonColor', 'jsonAlamatCustomer', 'jsonSeleraKonsumen'));
    }
}
