<?php

namespace App\Http\Controllers;

use App\NilaiAtribut;
use Illuminate\Http\Request;
use App\Library\NaiveBayes;

class PerhitunganController extends Controller
{
    private $linkView = 'modul.perhitungan.';

    public function __construct()
    {
        $this->middleware('Admin');
    }

    public function index()
    {
        $var['produk'] = NilaiAtribut::where('nama_atribut', 'Produk')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['color'] = NilaiAtribut::where('nama_atribut', 'Color')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['alamat_customer'] = NilaiAtribut::where('nama_atribut', 'Alamat Customer')->pluck('nilai_atribut', 'nilai_atribut')->all();

        return view($this->linkView.'perhitungan', compact('var'));
    }

    public function hitung(Request $request)
    {
        $var['produk'] = NilaiAtribut::where('nama_atribut', 'Produk')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['color'] = NilaiAtribut::where('nama_atribut', 'Color')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['alamat_customer'] = NilaiAtribut::where('nama_atribut', 'Alamat Customer')->pluck('nilai_atribut', 'nilai_atribut')->all();
        
        $var['nilai_produk'] = $request->produk;
        $var['nilai_color'] = $request->color;
        $var['nilai_alamat_customer'] = $request->alamat_customer;

        $nb = new NaiveBayes;
        $nb->setFieldClass('selera_konsumen');        
        $nb->setJumlahData();
        $nb->setJumlahPerClass();
        $nb->setProbabilitasPerClass();

        $nb->setJumlahPerParameterClass('produk', $request->produk);             
        $nb->setJumlahPerParameterClass('color', $request->color);         
        $nb->setJumlahPerParameterClass('alamat_customer', $request->alamat_customer);           

        $nb->setProbabilitasPerParameterClass();
        $nb->setProbabilitasMultiParameter();
        $nb->setProbabilitasFinal();

        $var['proses'] = $nb->printProcess();    
        $hasil = explode("|", $nb->output());

        $var['hasil'] = "<pre>Hasilnya : Produk ".ucwords(strtolower($request->produk))." berwarna ".ucwords(strtolower($request->color))." ";
        $var['hasil'] .= "untuk penjualan daerah ".ucwords(strtolower($request->alamat_customer))." masuk dalam kategori <b>".ucwords(strtolower($hasil[0]))."</b> ";
        $var['hasil'] .= "dengan nilai terbesar : <b>".$hasil[1]."</b></pre>";

        return view($this->linkView.'perhitungan', compact('var'));
    }

}
