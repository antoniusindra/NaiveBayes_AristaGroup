<?php

namespace App\Http\Controllers;

use App\NilaiAtribut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class NilaiAtributController extends Controller
{
    private $linkView = 'modul.nilai-atribut.';
    private $cari;

    public function __construct()
    {
        $this->middleware('Admin');
        $this->cari = Input::get('cari', '');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['lihat-data'] = 'active';
        $var['cari'] = (empty($this->cari)?'':'cari='.$this->cari);

        $query = NilaiAtribut::orderBy('id', 'desc');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listNilaiAtribut = $query->paginate(10);
        (!empty($this->cari))?$listNilaiAtribut->setPath('nilai-atribut?'.$var['cari']):'';

        return view($this->linkView.'table', compact('var', 'listNilaiAtribut'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['form-data'] = 'active';
        $var['method'] = 'create';

        return view($this->linkView.'form', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            NilaiAtribut::create($request->all());
            session()->flash('pesanProses', 'Data Nilai Atribut Berhasil Disimpan ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Data Nilai Atribut Gagal Disimpan ... :(');
        }
        return redirect('nilai-atribut/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $var['form-data'] = 'active';
        $var['method'] = 'edit';

        $listNilaiAtribut = NilaiAtribut::find($id);

        return view($this->linkView.'form', compact('var', 'listNilaiAtribut'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            NilaiAtribut::find($id)->update($request->all());
            session()->flash('pesanProses', 'Data Nilai Atribut Berhasil Diupdate ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Data Nilai Atribut Gagal Diupdate ... :(');
        }

        return redirect('nilai-atribut');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            NilaiAtribut::find($id)->delete();
            session()->flash('pesanProses', 'Data Nilai Atribut Berhasil Dihapus ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Data Nilai Atribut Gagal Dihapus ... :(');
        }

        return redirect('nilai-atribut');
    }
}
