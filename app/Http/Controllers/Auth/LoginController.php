<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('AfterLogin', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        if($request->username == 'admin' && $request->password == 'admin'){
            session(['admin' => 'admin']);
            return redirect('/beranda');
        }

        return redirect('/')
            ->withInput(Input::except('password'))
            ->withErrors(['gagal' => 'Opss ... Login gagal.']);
    }

    public function logout()
    {
        session()->flush();
        return redirect('/');
    }
}
