<?php

namespace App\Http\Controllers;

use App\Dataset;
use App\NilaiAtribut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DatasetController extends Controller
{
    private $linkView = 'modul.dataset.';
    private $cari;

    public function __construct()
    {
        $this->middleware('Admin');
        $this->cari = Input::get('cari', '');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['lihat-data'] = 'active';
        $var['cari'] = (empty($this->cari)?'':'cari='.$this->cari);

        $query = Dataset::orderBy('id', 'desc');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listDataset = $query->paginate(10);
        $jumDataSemua = $query->count();
        (!empty($this->cari))?$listDataset->setPath('dataset?'.$var['cari']):'';



        return view($this->linkView.'table', compact('var', 'listDataset', 'jumDataSemua'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['form-data'] = 'active';
        $var['method'] = 'create';
        $var['produk'] = NilaiAtribut::where('nama_atribut', 'Produk')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['color'] = NilaiAtribut::where('nama_atribut', 'Color')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['alamat_customer'] = NilaiAtribut::where('nama_atribut', 'Alamat Customer')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['selera_konsumen'] = NilaiAtribut::where('nama_atribut', 'Selera Konsumen')->pluck('nilai_atribut', 'nilai_atribut')->all();

        return view($this->linkView.'form', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Dataset::create($request->all());
            session()->flash('pesanProses', 'Dataset Berhasil Disimpan ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Dataset Gagal Disimpan ... :(');
        }
        return redirect('dataset/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['form-data'] = 'active';
        $var['method'] = 'edit';
        $var['produk'] = NilaiAtribut::where('nama_atribut', 'Produk')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['color'] = NilaiAtribut::where('nama_atribut', 'Color')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['alamat_customer'] = NilaiAtribut::where('nama_atribut', 'Alamat Customer')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $var['selera_konsumen'] = NilaiAtribut::where('nama_atribut', 'Selera Konsumen')->pluck('nilai_atribut', 'nilai_atribut')->all();
        $listDataset = Dataset::find($id);

        return view($this->linkView.'form', compact('var', 'listDataset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Dataset::find($id)->update($request->all());
            session()->flash('pesanProses', 'Dataset Berhasil Diupdate ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Dataset Gagal Diupdate ... :(');
        }

        return redirect('dataset');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Dataset::find($id)->delete();
            session()->flash('pesanProses', 'Dataset Berhasil Dihapus ... :)');
        } catch (\Exception $e) {
            session()->flash('pesanError', 'Dataset Gagal Dihapus ... :(');
        }

        return redirect('dataset');
    }
}
