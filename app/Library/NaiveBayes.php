<?php
namespace App\Library;

use App\Dataset;

class NaiveBayes 
{
	private $fieldClass;
	private $jumlahData;
	private $jumlahPerClass;
	private $jumlahPerParameterClass;
	private $probabilitasPerClass;
	private $probabilitasPerParameterClass;
	private $probabilitasMultiParameter;
	private $probabilitasFinal;
	private $output;
	private $listParameter;

	public function pembulatan($value){
		return round($value, 4);
	}

	public function setFieldClass($field){
		$this->fieldClass = $field;
	}

	public function pecah($string){
		$kata = explode("|", $string);
		return $kata;
	}

	public function setJumlahData(){
		$jumlah = Dataset::count();
		$this->jumlahData = $jumlah;
	}

	public function getJumlahData(){
		return $this->jumlahData;
	}

	public function setJumlahPerClass(){
		$listDataClass = Dataset::selectRaw('distinct selera_konsumen, count(*)as jumlah')->groupBy('selera_konsumen')->get();
		foreach ($listDataClass as $view) {
			$this->jumlahPerClass[$view->selera_konsumen] = $view->jumlah;
		}
	}

	public function getJumlahPerClass(){
		return $this->jumlahPerClass;
	}

	public function setProbabilitasPerClass(){
		foreach ($this->jumlahPerClass as $key => $value) {
			$nilai = $value/$this->jumlahData;
			$this->probabilitasPerClass[$key] = $this->pembulatan($nilai);
		}
	}

	public function getProbabilitasPerClass(){
		return $this->probabilitasPerClass;
	}

	public function setJumlahPerParameterClass($field, $data){
		foreach ($this->jumlahPerClass as $key => $value){
			$jumlah = Dataset::where($field, $data)
					->where($this->fieldClass, $key)
					->count();

			$this->jumlahPerParameterClass[$data." | ".$key] = $jumlah;
			$this->listParameter[$field] = $data;
		}
	}

	public function getJumlahPerParameterClass(){
		return $this->jumlahPerParameterClass;
	}

	public function setProbabilitasPerParameterClass(){
		foreach ($this->jumlahPerParameterClass as $key => $value){
			$jenis = $this->pecah($key);
			$parameter = trim($jenis[1]);

			$nilai = $value/$this->jumlahPerClass[$parameter];
			$this->probabilitasPerParameterClass[$key] = $this->pembulatan($nilai);
		}
	}

	public function getProbabilitasPerParameterClass(){
		return $this->probabilitasPerParameterClass;
	}

	public function setProbabilitasMultiParameter(){
		foreach ($this->probabilitasPerClass as $key => $value){
			$nilai = 1;
			foreach($this->probabilitasPerParameterClass as $secondKey => $secondValue){
				$jenis = $this->pecah($secondKey);
				$parameter = trim($jenis[1]);

				if($key==$parameter){
					$nilai *= $secondValue;
				}
			}
			$this->probabilitasMultiParameter[$key] = $this->pembulatan($nilai);
		}
	}

	public function getProbabilitasMultiParameter(){
		return $this->probabilitasMultiParameter;
	}

	public function setProbabilitasFinal(){
		foreach ($this->probabilitasPerClass as $key => $value){
			$nilai = $value*$this->probabilitasMultiParameter[$key];
			$this->probabilitasFinal[$key] = $this->pembulatan($nilai);
		}
	}

	public function getProbabilitasFinal(){
		return $this->probabilitasFinal;
	}

	public function output(){
		foreach($this->probabilitasFinal as $key => $value){
			if(empty($nilai)){
				$nilai = $value;
				$this->output = $key."|".$value;
			}else if($nilai < $value){
				$nilai = $value;
				$this->output = $key."|".$value;
			}
		}

		return $this->output;
	}

	public function printProcess(){
		$no = 0;
		$str = "<pre>";
		$str .= "Terdapat ".count($this->jumlahPerClass)." class dari klasifikasi yang dibentuk yaitu : <br>";
		foreach ($this->jumlahPerClass as $key => $value){
			$no++;
			$str .= "C".$no." => ".$this->fieldClass." = ".$key."<br>";
		}
		$str .= "<br>";
		$str .= "Pencarian klasifikasi dengan data yang belum diketahui kelasnya.<br>";
		$str .= "X = ("; 
		$no = 0;
		foreach ($this->listParameter as $key => $value){
			$no++;

			if($no == count($this->listParameter)){
				$str .= $key." = ".$value."";
			}else{
				$str .= $key." = ".$value.", ";
			}
		}
		$str .= ")<br><br>";

		$str .= "Mencari P(Ci) atau prior probability untuk setiap class berdasar data. <br>";
		foreach ($this->jumlahPerClass as $key => $value) {
			$str .= "P(".$this->fieldClass." = ".$key.") = ".$value."/".$this->jumlahData." = ".$this->probabilitasPerClass[$key]."<br>";
		}

		$str .= "<br>";
		$str .= "Hitung P(X|Ci). <br>";

		foreach ($this->listParameter as $key => $value){
			foreach($this->jumlahPerClass as $secondKey => $secondValue){
				$str .= "P(".$key." = ".$value." | ".$this->fieldClass." = ".$secondKey.") = ";
				$str .= $this->jumlahPerParameterClass[$value." | ".$secondKey]."/".$secondValue." = ";
				$str .= $this->probabilitasPerParameterClass[$value." | ".$secondKey]."<br>";
			}
		}

		$str .= "<br>";

		foreach ($this->jumlahPerClass as $key => $value){
			$no=0;
			$str .= "P(X | ".$this->fieldClass." = ".$key.") = ";
			foreach($this->listParameter as $secondKey => $secondValue){
				$no++;
				if($no == count($this->listParameter)){
					$str .= $this->probabilitasPerParameterClass[$secondValue." | ".$key]." = ";
				}else{
					$str .= $this->probabilitasPerParameterClass[$secondValue." | ".$key]." *";
				}
			}
			$str .= $this->probabilitasMultiParameter[$key];
			$str .= "<br>";
		}

		$str .= "<br>";

		foreach ($this->probabilitasPerClass as $key => $value){
			$str .= "P(X | ".$this->fieldClass." = ".$key.") * P(".$this->fieldClass." = ".$key.") = ";
			$str .= $this->probabilitasMultiParameter[$key]."*".$value." = ".$this->probabilitasFinal[$key]."<br>";
		}

		$str .= "</pre>";

		return $str;
	}
}



