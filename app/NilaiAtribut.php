<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiAtribut extends Model
{
    protected $table = 'nilai_atribut';
    protected $fillable = ['nama_atribut', 'nilai_atribut'];
    public $timestamps = false;

    public function scopeCari($query, $cari) {
    	$query->where('nama_atribut', 'like', '%'.$cari.'%')
        	->orWhere('nilai_atribut', 'like', '%'.$cari.'%');
    }
}
