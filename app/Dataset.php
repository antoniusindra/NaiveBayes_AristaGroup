<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    protected $table = 'dataset';
    protected $fillable = ['produk', 'color', 'alamat_customer', 'selera_konsumen'];
    public $timestamps = false;

    public function scopeCari($query, $cari) {
    	$query->where('produk', 'like', '%'.$cari.'%')
        	->orWhere('color', 'like', '%'.$cari.'%')
        	->orWhere('alamat_customer', 'like', '%'.$cari.'%')
        	->orWhere('selera_konsumen', 'like', '%'.$cari.'%');
    }
}
