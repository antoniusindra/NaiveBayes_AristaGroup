<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('beranda', 'BerandaController@index')->name('beranda');
Route::resource('nilai-atribut', 'NilaiAtributController');
Route::resource('dataset', 'DatasetController');
Route::get('perhitungan', 'PerhitunganController@index');
Route::post('perhitungan', 'PerhitunganController@hitung')->name('perhitungan.hitung');
