var Script = function () {

    // $.validator.setDefaults({
    //     submitHandler: function() { alert("submitted!"); }
    // });

    $.validator.addMethod("alphanumspace", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z0-9\s]+$/);
    }, "Hanya boleh spasi, huruf dan angka");

    $().ready(function() {

        // validate the comment form when it is submitted
        $("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            }
        });

        // propose username by combining first- and lastname
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if(firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        // validate form nilai atribut
        $("#formNilaiAtribut").validate({
            rules: {
                nama_atribut: {
                    required: true
                },
                nilai_atribut: {
                    required: true,
                    alphanumspace: true
                }
            },
            messages: {
                nama_atribut: {
                    required: "Kolom nama atribut harus diisi"
                },
                nilai_atribut: {
                    required: "Kolom nilai atribut harus diisi"
                }
            }
        });

         // validate form dataset
        $("#formDataset").validate({
            rules: {
                produk: "required",
                color: "required",
                alamat_customer: "required",
                selera_konsumen: "required"
            },
            messages: {
                produk: "Kolom produk harus diisi",
                color: "Kolom color harus diisi",
                alamat_customer: "Kolom alamat customer harus diisi",
                selera_konsumen: "Kolom selera konsumen harus diisi"
            }
        });

        // validate form perhitungan
        $("#formPerhitungan").validate({
            rules: {
                produk: "required",
                color: "required",
                alamat_customer: "required"            },
            messages: {
                produk: "Kolom produk harus diisi",
                color: "Kolom color harus diisi",
                alamat_customer: "Kolom alamat customer harus diisi"            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
    });


}();