@extends('layouts.admin')

@section('content')
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Perhitungan
    </h3>
</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
	<div class="row">
	    <div class="col-md-12">
	        <section class="panel">
	            <div class="panel-body">
	                <div class="tab-content">
	                    <div class="tab-pane active">
							{!! Form::open(['id'=>'formPerhitungan', 'method'=>'POST', 'route'=>'perhitungan.hitung', 'class'=>'cmxform form-horizontal adminex-form']) !!}
								<div class="form-group">
									{!! Form::label('produk', 'Produk', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
						            <div class="col-sm-10">
						            	{!! Form::select('produk', $var['produk'], (!empty($var['nilai_produk'])?$var['nilai_produk']:null), 
						            		['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
						            </div>
						        </div>
						        <div class="form-group">
									{!! Form::label('color', 'Color', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
						            <div class="col-sm-10">
						            	{!! Form::select('color', $var['color'], (!empty($var['nilai_color'])?$var['nilai_color']:null),
						            		['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
						            </div>
						        </div>
						        <div class="form-group">
									{!! Form::label('alamat_customer', 'Alamat Customer', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
						            <div class="col-sm-10">
						            	{!! Form::select('alamat_customer', $var['alamat_customer'], (!empty($var['nilai_alamat_customer'])?$var['nilai_alamat_customer']:null), 
						            		['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
						            </div>
						        </div>
						        <div class="form-group form-actions">
							        <div class="col-md-12 col-md-offset-9">
							            {!! Form::submit('Hitung', ['class'=>'btn btn-primary']) !!}
							            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
							        </div>
						    	</div>
							{!! Form::close() !!}
							<br />

							@if(!empty($var['hasil']))
								<legend>Perhitungan</legend>
								{!! $var['proses'] !!}

								<br />
								<legend>Hasil Perhitungan</legend>
								{!! $var['hasil'] !!}
							@endif
	                    </div>
	                </div>
	            </div>
	        </section>
	    </div>
	</div>
</div>
<!--body wrapper end-->
@endsection