@extends('modul.nilai-atribut.nilai-atribut')

@section('content-2')
	<div class="col-md-6 col-md-offset-6">
        <form method="GET" action="">
            <div class="input-group m-bot15">
                <input name="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Cari</button>
                </span>
            </div>
        </form>
    </div>

    <div class="table-responsive">
        <table  class="display table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th width="70px"><center>Aksi</center></th>
                <th><center>Nama Atribut</center></th>
                <th><center>Nilai Atribut</center></th>
            </tr>
            </thead>
            <tbody>
            @foreach($listNilaiAtribut as $view)
                <tr>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'route'=> ['nilai-atribut.destroy', $view->id], 'onSubmit'=>'return confirm ("Yakin data akan dihapus ?")']) !!}
                        <div class="btn-group">
                            {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit','class'=>'btn btn-danger btn-xs']) !!}
                            <a href="{{ url('/nilai-atribut/'.$view->id.'/edit')}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                        </div>
                        {!! Form::close() !!}
                    </td>
                    <td>{{ $view->nama_atribut }}</td>
                    <td>{{ $view->nilai_atribut }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="col-md-6 col-md-offset-6">
        {{ $listNilaiAtribut->render() }}
    </div>
@endsection