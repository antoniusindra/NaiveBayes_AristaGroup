@extends('modul.nilai-atribut.nilai-atribut')

@section('content-2')
	@if($var['method']=='edit')
		{!! Form::model($listNilaiAtribut, ['id'=>'formNilaiAtribut', 'method'=>'PATCH', 'route'=> ['nilai-atribut.update', $listNilaiAtribut->id], 'class'=>'cmxform form-horizontal adminex-form']) !!}
	@elseif($var['method']=='create')
		{!! Form::open(['method'=>'POST', 'id'=>'formNilaiAtribut', 'route'=>'nilai-atribut.store', 'class'=>'cmxform form-horizontal adminex-form']) !!}
	@endif
		<div class="form-group">
			{!! Form::label('nama_atribut', 'Nama Atribut', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::select('nama_atribut', ['Produk' => 'Produk', 'Color' => 'Color', 'Alamat Customer' => 'Alamat Customer', 'Selera Konsumen' => 'Selera Konsumen'],
                    null, ['class'=>'form-control', 'placeholder'=>'Pilih Nama Atribut']) !!}
            </div>
        </div>
		<div class="form-group">
			{!! Form::label('nilai_atribut', 'Nilai Atribut', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::text('nilai_atribut', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nilai Atribut']) !!}
            </div>
        </div>
        <div class="form-group form-actions">
	        <div class="col-md-12 col-md-offset-9">
	            @if($var['method']=='edit')
	                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
	            @elseif($var['method']=='create')
	                {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
	            @endif
	            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
	        </div>
    	</div>
	{!! Form::close() !!}
@endsection