@extends('layouts.admin')

@section('content')
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dataset
    </h3>
</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
	<div class="row">
	    <div class="col-md-12">
	        <section class="panel">
	            <header class="panel-heading custom-tab dark-tab">
	                <ul class="nav nav-tabs">
	                    <li class="{{ (!empty($var['lihat-data'])?$var['lihat-data']:'') }}">
	                        <a href="{{ url('dataset') }}">Lihat Data</a>
	                    </li>
	                    <li class="{{ (!empty($var['form-data'])?$var['form-data']:'') }}">
	                        <a href="{{ url('dataset/create') }}">Form Data</a>
	                    </li>
	                </ul>
	            </header>
	            <div class="panel-body">
	                <div class="tab-content">
	                    <div class="tab-pane active">
	                        @yield('content-2')
	                    </div>
	                </div>
	            </div>
	        </section>
	    </div>
	</div>
</div>
<!--body wrapper end-->
@endsection