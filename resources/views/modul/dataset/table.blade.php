@extends('modul.dataset.dataset')

@section('content-2')
    <div class="col-md-6">
        <div class="input-group m-bot15">
            <label class="col-lg-5 col-sm-5 control-label">Jumlah Data</label>
            <div class="col-lg-7 col-sm-7">
                <input name="cari" type="text" class="form-control" value="{{ $jumDataSemua }}" readonly>
            </div>
        </div>
    </div>
	<div class="col-md-6">
        <form method="GET" action="">
            <div class="input-group m-bot15">
                <input name="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Cari</button>
                </span>
            </div>
        </form>
    </div>

    <div class="table-responsive">
        <table  class="display table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th width="70px"><center>Aksi</center></th>
                <th><center>Produk</center></th>
                <th><center>Color</center></th>
                <th><center>Alamat Customer</center></th>
                <th><center>Selera Konsumen</center></th>
            </tr>
            </thead>
            <tbody>
            @foreach($listDataset as $view)
                <tr>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'route'=> ['dataset.destroy', $view->id], 'onSubmit'=>'return confirm ("Yakin data akan dihapus ?")']) !!}
                        <div class="btn-group">
                            {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit','class'=>'btn btn-danger btn-xs']) !!}
                            <a href="{{ url('/dataset/'.$view->id.'/edit')}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                        </div>
                        {!! Form::close() !!}
                    </td>
                    <td>{{ $view->produk }}</td>
                    <td>{{ $view->color }}</td>
                    <td>{{ $view->alamat_customer }}</td>
                    <td>{{ $view->selera_konsumen }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="col-md-6 col-md-offset-6">
        {{ $listDataset->render() }}
    </div>
@endsection