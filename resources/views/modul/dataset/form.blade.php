@extends('modul.dataset.dataset')

@section('content-2')
	@if($var['method']=='edit')
		{!! Form::model($listDataset, ['id'=>'formDataset', 'method'=>'PATCH', 'route'=> ['dataset.update', $listDataset->id], 'class'=>'cmxform form-horizontal adminex-form']) !!}
	@elseif($var['method']=='create')
		{!! Form::open(['id'=>'formDataset', 'method'=>'POST', 'route'=>'dataset.store', 'class'=>'cmxform form-horizontal adminex-form']) !!}
	@endif
		<div class="form-group">
			{!! Form::label('produk', 'Produk', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::select('produk', $var['produk'], null, ['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
            </div>
        </div>
        <div class="form-group">
			{!! Form::label('color', 'Color', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::select('color', $var['color'], null, ['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
            </div>
        </div>
        <div class="form-group">
			{!! Form::label('alamat_customer', 'Alamat Customer', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::select('alamat_customer', $var['alamat_customer'], null, ['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
            </div>
        </div>
        <div class="form-group">
			{!! Form::label('selera_konsumen', 'Selera Konsumen', ['class' => 'col-lg-2 col-sm-2 control-label']) !!}
            <div class="col-sm-10">
            	{!! Form::select('selera_konsumen', $var['selera_konsumen'], null, ['class'=>'form-control', 'placeholder'=>'Pilih Nilai Atribut']) !!}
            </div>
        </div>
        <div class="form-group form-actions">
	        <div class="col-md-12 col-md-offset-9">
	            @if($var['method']=='edit')
	                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
	            @elseif($var['method']=='create')
	                {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
	            @endif
	            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
	        </div>
    	</div>
	{!! Form::close() !!}
@endsection