@extends('layouts.admin')

@section('content')
	<!-- page heading start-->
	<div class="page-heading">
	    <h3>
	        Beranda
	    </h3>
	</div>
	<!-- page heading end-->

	<div class="wrapper">
		<div class="row">
			<div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                		<div id="container-produk" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                		<div id="container-color" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                		<div id="container-alamat-customer" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                		<div id="container-selera-konsumen" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>

	@section('javascript')
		<script type="text/javascript">
			$(function () {
				Highcharts.chart('container-produk', {
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Grafik Jumlah Data Atribut Produk'
			        },
			        xAxis: {
			            labels: {
						  enabled: false
						}
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Jumlah'
			            },
			        },
			        tooltip: {
			            headerFormat: '<span style="font-size:10px">Keterangan</span><table>',
			            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			            footerFormat: '</table>',
			            useHTML: true
			        },
			        plotOptions: {
			            column: {
			                pointPadding: 0.2,
			                borderWidth: 0
			            }
			        },
			        series: {!! $jsonProduk !!}
			    });

				Highcharts.chart('container-color', {
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Grafik Jumlah Data Atribut Color'
			        },
			        xAxis: {
			            labels: {
						  enabled: false
						}
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Jumlah'
			            },
			        },
			        tooltip: {
			            headerFormat: '<span style="font-size:10px">Keterangan</span><table>',
			            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			            footerFormat: '</table>',
			            useHTML: true
			        },
			        plotOptions: {
			            column: {
			                pointPadding: 0.2,
			                borderWidth: 0
			            }
			        },
			        series: {!! $jsonColor !!}
			    });

			    Highcharts.chart('container-alamat-customer', {
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Grafik Jumlah Data Atribut Alamat Customer'
			        },
			        xAxis: {
			            labels: {
						  enabled: false
						}
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Jumlah'
			            },
			        },
			        tooltip: {
			            headerFormat: '<span style="font-size:10px">Keterangan</span><table>',
			            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			            footerFormat: '</table>',
			            useHTML: true
			        },
			        plotOptions: {
			            column: {
			                pointPadding: 0.2,
			                borderWidth: 0
			            }
			        },
			        series: {!! $jsonAlamatCustomer !!}
			    });

			    Highcharts.chart('container-selera-konsumen', {
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Grafik Jumlah Data Atribut Selera Konsumen'
			        },
			        xAxis: {
			            labels: {
						  enabled: false
						}
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Jumlah'
			            },
			        },
			        tooltip: {
			            headerFormat: '<span style="font-size:10px">Keterangan</span><table>',
			            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			            footerFormat: '</table>',
			            useHTML: true
			        },
			        plotOptions: {
			            column: {
			                pointPadding: 0.2,
			                borderWidth: 0
			            }
			        },
			        series: {!! $jsonSeleraKonsumen !!}
			    });
			});
		</script>
		<script src="{{ asset('vendor/highchart-5.06/code/highcharts.js') }}"></script>
	@endsection
@endsection