<!DOCTYPE html>
<html lang="en">
<head>
    @include('include/head')
</head>

<body class="login-body">

    <div class="container">

        {!! Form::open(['method'=>'POST', 'route'=>'login', 'class'=>'form-signin', 'id'=>'formLogin']) !!}
            <div class="form-signin-heading text-center">
                <h1 class="sign-title">Sign In</h1>
                <img src="{{ asset('adminex/images/login-logo-2.png') }}" alt=""/>
            </div>

            <div class="login-wrap">
                @if ($errors->has('gagal'))
                    <div class="alert alert-danger">
                        {{ $errors->first('gagal') }}
                    </div>
                @endif
                
                {!! Form::text('username', old('username'), ['class'=>'form-control', 'placeholder'=>'Inputkan Username', 'id'=>'username', 'required']) !!}
                {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Inputkan Password', 'id'=>'password', 'required']) !!}

                <button class="btn btn-lg btn-login btn-block" type="submit">
                    <i class="fa fa-check"></i>
                </button>
            </div>
        {!! Form::close() !!}

    </div>

    @include('include/javascript')

</body>
</html>
