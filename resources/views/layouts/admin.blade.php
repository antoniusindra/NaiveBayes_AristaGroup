<!DOCTYPE html>
<html lang="en">
<head>
    @include('include/head')
</head>

<body class="horizontal-menu-page">

    <section>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="{{ asset('adminex/images/logo-2.png') }}" alt="">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('beranda') }}">Beranda</a></li>
                        <li><a href="{{ url('nilai-atribut') }}">Nilai Atribut</a></li>
                        <li><a href="{{ url('dataset') }}">Dataset</a></li>
                        <li><a href="{{ url('perhitungan') }}">Perhitungan</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                Log Out
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        @include('include/alert')
        
        @yield('content')

        @include('include/footer')

    </section>

    @include('include/javascript')

</body>
</html>
