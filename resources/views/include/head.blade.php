<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="{{ asset('adminex/images/icon.png') }}" >

<title>{{ config('app.name', 'Laravel') }}</title>

<link href="{{ asset('adminex/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('adminex/css/style-responsive.css') }}" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<style type="text/css">
	.pagination>.active>span{
	    background-color: #6dc5a3;
	    border-color: #6dc5a3;
	}

	.pagination>.active>span:hover{
	    background-color: #6dc5a3;
	    border-color: #6dc5a3;
	}
</style>