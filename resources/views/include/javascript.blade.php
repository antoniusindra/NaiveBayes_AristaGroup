<!-- Placed js at the end of the document so the pages load faster -->
<script src="{{ asset('adminex/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('adminex/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
<script src="{{ asset('adminex/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('adminex/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminex/js/modernizr.min.js') }}"></script>
<script src="{{ asset('adminex/js/jquery.nicescroll.js') }}"></script>

<script type="text/javascript" src="{{ asset('adminex/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('adminex/js/validation-init.js') }}"></script>

<!--common scripts for all pages-->
<script src="{{ asset('adminex/js/scripts.js') }}"></script>

@yield('javascript')