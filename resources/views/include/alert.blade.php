@if(Session::has('pesanProses'))
    <div class="alert alert-success fade in">
        <button type="button" class="close close-sm" data-dismiss="alert">
            <i class='fa fa-times'></i>
        </button>
        {{ Session::get('pesanProses') }}
    </div>
@endif

@if(Session::has('pesanError'))
    <div class="alert alert-danger fade in">
        <button type="button" class="close close-sm" data-dismiss="alert">
            <i class='fa fa-times'></i>
        </button>
        {{ Session::get('pesanError') }}
    </div>
@endif